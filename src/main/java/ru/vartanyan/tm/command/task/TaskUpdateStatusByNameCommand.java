package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskUpdateStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-status-by-name";
    }

    @Override
    public String description() {
        return "Update task status by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK STATUS]");
        System.out.println("[ENTER NAME]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final String userId = serviceLocator.getAuthService().getUserId();
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = serviceLocator.getTaskService().updateEntityStatusByName(name, status, userId);
        System.out.println("[TASK STATUS UPDATED]");
    }

}
