package ru.vartanyan.tm.command.system;

import ru.vartanyan.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "show-version";
    }

    @Override
    public String description() {
        return "Show version";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
