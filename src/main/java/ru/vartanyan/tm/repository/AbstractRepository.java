package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public void removeById(final String id) throws Exception {
        remove(entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public E findById(final String id) {
        return entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

}
