package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return entities.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeByLogin(final String login) {
        remove(entities.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null)
        );
    }

}
