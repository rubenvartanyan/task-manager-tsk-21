package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String id,
                      final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void clear(final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(entities::remove);
    }

    @Override
    public void removeById(final String id,
                           final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Override
    public E findOneByIndex(final Integer index,
                            final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList())
                .get(index);
    }

    @Override
    public E findOneByName(final String name,
                           final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeOneByIndex(final Integer index,
                                 final String userId) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList())
                .get(index)
        );
    }

    @Override
    public void removeOneByName(final String name,
                                final String userId) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator,
                           final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public void showEntity(final E entity) {
        System.out.println("[NAME: " + entity.getName() + " ]");
        System.out.println("[DESCRIPTION: " + entity.getDescription() + " ]");
        System.out.println("STATUS: " + entity.getStatus() + " ]");
    }

}
