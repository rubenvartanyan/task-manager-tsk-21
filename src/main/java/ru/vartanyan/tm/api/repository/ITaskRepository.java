package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    //List<Task> findAll(Comparator<Task> comparator, String userId);

    List<Task> findAllByProjectId(String projectId, String userId);

    List<Task> removeAllByProjectId(String projectId, String userId);

    Task bindTaskByProjectId(String projectId, String taskId, String userId);

    Task unbindTaskFromProject(String projectId, String taskId, String userId);

    //Task findOneByIndex(Integer index,
      //                  String userId);

    //Task findOneByName(String name, String userId);

    //void removeOneByIndex(Integer index, String userId);

    //Task removeOneByName(String name, String userId);

}
