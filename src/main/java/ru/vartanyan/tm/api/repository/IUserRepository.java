package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public interface IUserRepository extends IRepository<User>{


    User findByLogin(final String login);

    void removeByLogin(final String login);

}
