package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

   AbstractCommand getCommandByName(String name);

   AbstractCommand getCommandByArg(String arg);

   Collection<AbstractCommand> getArguments();

    Collection<AbstractCommand> getCommands();

   Collection<String> getListArgumentName();

    Collection<String> getListCommandName();

   void add(AbstractCommand command);

}
