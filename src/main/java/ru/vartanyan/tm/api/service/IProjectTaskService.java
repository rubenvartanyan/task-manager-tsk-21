package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // SHOW ALL TASKS FROM PROJECT
    List<Task> findAllTaskByProjectId(String projectId, String userId) throws Exception;

    // ADD TASK TO PROJECT
    Task bindTaskByProjectId(String projectId, String taskId, String userId) throws Exception;

    // REMOVE TASK FROM PROJECT
    Task unbindTaskFromProject(String projectId, String taskId, String userId) throws EmptyIdException, Exception;

    // REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT
    void removeProjectById(String projectId, String userId) throws Exception;

}
