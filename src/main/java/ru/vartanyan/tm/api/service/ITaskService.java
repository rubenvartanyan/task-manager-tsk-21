package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    Task add(String name, String description,
                    final String userId) throws Exception;

}
