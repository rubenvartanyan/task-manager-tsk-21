package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {

    E add(final E entity);

    void remove(final E entity);

    E findById(final String id) throws Exception;

    void removeById(final String id) throws Exception;

}
