package ru.vartanyan.tm.api;

import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.AbstractBusinessEntity;
import ru.vartanyan.tm.model.Project;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IBusinessRepository<E>, IService<E> {

    E add(String name, String description, String userId) throws Exception;

    E updateEntityById(String id, String name, String description, String userId) throws Exception;

    E updateEntityByIndex(Integer index, String name, String description, String userId) throws EmptyNameException, Exception;

    E startEntityById(String id, String userId) throws Exception;

    E startEntityByName(String name, String userId) throws Exception;

    E startEntityByIndex(Integer index, String userId) throws Exception;

    E finishEntityById(String id, String userId) throws Exception;

    E finishEntityByName(String name, String userId) throws Exception;

    E finishEntityByIndex(Integer index, String userId) throws Exception;

    E updateEntityStatusById(String id, Status status, String userId) throws Exception;

    E updateEntityStatusByName(String name, Status status, String userId) throws Exception;

    E updateEntityStatusByIndex(Integer index, Status status, String userId) throws Exception;

    void showEntityByName(final String name, final String userId) throws Exception;

}
