package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.Task;

public class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(String name, String description,
                    final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        return task;
    }


}
