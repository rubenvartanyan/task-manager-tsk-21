package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService{

    final private ITaskRepository taskRepository;

    final private IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId,
                                             final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    @Override
    public Task bindTaskByProjectId(final String projectId,
                                    final String taskId,
                                    final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        return taskRepository.bindTaskByProjectId(projectId, taskId, userId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId,
                                      final String taskId,
                                      final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        return taskRepository.unbindTaskFromProject(projectId, taskId, userId);
    }

    @Override
    public void removeProjectById(final String projectId,
                                  final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId, userId);
        projectRepository.removeById(projectId, userId);
    }

}
