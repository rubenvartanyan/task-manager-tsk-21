package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    private final IBusinessRepository<E> businessRepository;

    public AbstractBusinessService(final IBusinessRepository<E> businessRepository) {
        super(businessRepository);
        this.businessRepository = businessRepository;
    }
    @Override
    public E findById(final String id,
                      final String userId) {
        if (id == null || id.isEmpty()) return null;
        return businessRepository.findById(id, userId);
    }

    @Override
    public void clear(final String userId) {
        businessRepository.clear(userId);
    }

    @Override
    public void removeById(final String id,
                           final String userId) {
        if (id == null || id.isEmpty()) return;
        try {
            businessRepository.removeById(id, userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<E> findAll(final Comparator comparator,
                           final String userid) {
        if (comparator == null) return null;
        return businessRepository.findAll(comparator, userid);
    }

    @Override
    public E findOneByIndex(final Integer index,
                            final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        E entity = businessRepository.findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        return entity;
    }

    @Override
    public E findOneByName(final String name,
                           final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        E entity = businessRepository.findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        return entity;
    }

    @Override
    public void removeOneByIndex(final Integer index,
                                 final String userId) throws Exception{
        if (index < 0) throw new IncorrectIndexException(index);
        E entity = businessRepository.findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        remove(entity);
    }

    @Override
    public void removeOneByName(final String name,
                                final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        E entity = businessRepository.findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        remove(entity);
    }

    @Override
    public void showEntity(E entity) {
        businessRepository.showEntity(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        return businessRepository.findAll(userId);
    }

    @Override
    public E add(String name, String description, String userId) throws Exception {
        return null;
    }

    @Override
    public E updateEntityById(final String id,
                               final String name,
                               final String description,
                               final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateEntityByIndex(final Integer index,
                                  final String name,
                                  final String description,
                                  final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E startEntityById(final String id,
                              final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        entity.setDateStarted(dateStarted);
        return entity;
    }

    @Override
    public E startEntityByName(final String name,
                                final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        entity.setDateStarted(dateStarted);
        return entity;
    }

    @Override
    public E startEntityByIndex(final Integer index,
                                 final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        entity.setDateStarted(dateStarted);
        return entity;
    }

    @Override
    public E finishEntityById(final String id,
                               final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public E finishEntityByName(final String name,
                                 final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public E finishEntityByIndex(final Integer index,
                                  final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public E updateEntityStatusById(final String id,
                                     final Status status,
                                     final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(id, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E updateEntityStatusByName(final String name,
                                       final Status status,
                                       final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E updateEntityStatusByIndex(final Integer index,
                                        final Status status,
                                        final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        return entity;
    }

    public void showEntityByName(final String name, final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(name, userId);
        if (entity == null) throw new NullObjectException();
        businessRepository.showEntity(entity);
    }

}
