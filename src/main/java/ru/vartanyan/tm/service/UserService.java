package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.*;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.HashUtil;

import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login,
                       final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USER);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login,
                       final String password,
                       final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login,
                       String password,
                       Role role) throws Exception {
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(final String userId,
                            final String password) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(final String userId,
                           final String firstName,
                           final String lastName,
                           final String middleName
                           ) throws Exception {
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
