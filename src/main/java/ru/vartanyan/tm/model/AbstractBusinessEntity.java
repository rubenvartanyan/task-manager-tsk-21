package ru.vartanyan.tm.model;

import ru.vartanyan.tm.enumerated.Status;

import java.util.Date;

public class AbstractBusinessEntity extends AbstractEntity{

    public String name = "";

    public String description = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status status = Status.NOT_STARTED;

    public Date dateStarted;

    public Date dateFinish;

    public Date created = new Date();

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(Date dateStart) {
        this.dateStarted = dateStarted;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getId() + ": " + name + "; " + description + "; " + "User Id: " + getUserId();
    }

}
